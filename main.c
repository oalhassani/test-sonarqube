#include <stdio.h>
#include <math.h> // unused

// test file for Synk
void main() { // should be int main

    int a = 0; // unused
    volatile int b = 0x23421; // magic number
    char c = 'hello world\r\n'; // this should be a (char *) for a string
    printf("this is our bad message: %c", c);
    
    char c2[] = "this is a good message\r\n";
    printf("our good message: %s", c2);
    
    // should be return 0
    return 1;

    // dead code
    while(1);
}
